/***
 *
 * This file is part of the "wss_milestone" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

;(function(){
    var hive_thm_webfontloader__interval = setInterval(function () {

        if (typeof WebFont == 'undefined') {
        }  else {

            clearInterval(hive_thm_webfontloader__interval);

            if (hive_cfg_typoscript_sStage == "prototype" || hive_cfg_typoscript_sStage == "development") {
                console.info('use_webfontloader initialize');
            }

            hive_thm_webfontloader__webFontConfig = {
                google: {
                    families: ['Ubuntu:300,400,700']
                }
            };

            WebFont.load(hive_thm_webfontloader__webFontConfig);

        }

    }, 2000);
})();